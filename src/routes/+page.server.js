import { env } from '$env/dynamic/public';
import { redirect } from '@sveltejs/kit';

/** @type {import('./$types').PageServerLoad} */
export async function load() {
  const url = `${env.PUBLIC_BASE_URL}/login`
  throw redirect(307, url);
};