import db from '$lib/server/db';

import * as login from '$lib/server/models/login';
import { error, redirect } from '@sveltejs/kit';
import bcrypt from 'bcrypt';

/** @type {import('./$types').PageServerLoad} */
export async function load() {
  return {};
};

/** @type {import('./$types').Actions} */
export const actions = {
  default: async function ({ request, cookies, event }) {
    const formData = await request.formData();

    const username = formData.get('username');
    const password = formData.get('password');
    const rememberMe = formData.get('rememberMe');

    const userinfo = await login.getUserInfo(db, username);

    if (!userinfo) {
      throw error(400, {
        'message': 'Username not found!'
      })
    }

    const isMatch = bcrypt.compareSync(String(password), userinfo.password);

    if (!isMatch) {
      throw error(401, {
        'message': 'Password invalid'
      })
    }

    // set cookie
    cookies.set('sessionId', userinfo.user_id, {
      path: '/',
      httpOnly: true,
      sameSite: 'strict',
      secure: true,
      maxAge: 60 * 60 * 24 * 30
    });

    return { success: true }
  }
};