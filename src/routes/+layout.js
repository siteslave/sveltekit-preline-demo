import { env } from '$env/dynamic/public';

/** @type {import('./$types').LayoutLoad} */
export async function load() {
  const BASE_URL = env.PUBLIC_BASE_URL ?? '';
  return {
    BASE_URL,
  };
}