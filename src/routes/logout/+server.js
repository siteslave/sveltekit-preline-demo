import { redirect } from '@sveltejs/kit';

/** @type {import('./$types').RequestHandler} */
export async function GET({ cookies }) {
  cookies.delete('sessionId');
  throw redirect(302, '/login');
};