import { env } from '$env/dynamic/public';
import db from '$lib/server/db';
import * as userModel from '$lib/server/models/user';

function redirect(location, body) {
  return new Response(body, {
    status: 303,
    headers: { location }
  });
}

const unProtectedRoutes = [
  '/',
  '/login'
];

export const handle = async ({ event, resolve }) => {
  const sessionId = event.cookies.get('sessionId') ?? '';

  if (!sessionId && !unProtectedRoutes.includes(event.url.pathname))
    return redirect(`${env.PUBLIC_BASE_URL}/login`, 'No authenticated user.');

  /** @type {any} */
  let user = {};
  // get user

  if (sessionId) {
    user = await userModel.getUserInfoById(db, sessionId);
  }

  if (!user) {
    if (!unProtectedRoutes.includes(event.url.pathname)) return redirect(`${env.PUBLIC_BASE_URL}/login`, 'Not a valid user');
  }

  event.locals.user = user;

  return resolve(event);

};