export const getUserInfoById = (db, id) => {
  return db('users')
    .select('user_id', 'first_name', 'last_name')
    .where('user_id', id)
    .first();
}