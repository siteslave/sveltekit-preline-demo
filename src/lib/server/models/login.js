export const getUserInfo = (db, username) => {
  return db('users')
    .where('username', username)
    .first();
}