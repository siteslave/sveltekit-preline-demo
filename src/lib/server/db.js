import knex from 'knex';
import { env } from '$env/dynamic/private';

export default knex({
  client: 'pg',
  connection: {
    host: env.DB_HOST,
    port: 5432,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB_NAME
  },
  pool: {
    min: 2,
    max: 20
  }
})