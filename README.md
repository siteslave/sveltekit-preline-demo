# SvelteKit + Preline demo

## Creating a project

```
git clone https://gitlab.com/siteslave/sveltekit-preline-demo.git
```

## Create `run.sh` file:

```
DB_HOST=127.0.0.1 \
DB_USER=test \
DB_PASSWORD=test \
DB_NAME=test \
PUBLIC_BASE_URL=http://localhost:5173 \
npm run dev
```
## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
sh run.sh
```

## Building

```bash
npm run build
```
