-- public.users definition

-- Drop table

-- DROP TABLE users;

CREATE TABLE users (
	user_id uuid NOT NULL DEFAULT gen_random_uuid(),
	username varchar(50) NOT NULL,
	"password" varchar(200) NOT NULL,
	first_name varchar(100) NOT NULL,
	last_name varchar(200) NOT NULL,
	enabled bool NULL DEFAULT true,
	"role" varchar(10) NULL DEFAULT 'USER'::character varying,
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL,
	CONSTRAINT users_pk PRIMARY KEY (user_id)
);
CREATE INDEX users_enabled_idx ON users (enabled);
CREATE INDEX users_password_idx ON users ("password");
CREATE UNIQUE INDEX users_username_idx ON users (username);